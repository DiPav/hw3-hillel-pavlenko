package Hw3Package1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {
    public static final String SEAT = "Admin";
    public static final String EMAIL = "sss@gmail";
    public static final String PHONE_NUMBER = "+123";
    public static final int AGE = 11;
    public static final String FULL_NAME = "Victor Victorovich Victorov";

    @Test
    void shouldSetSeat() {
        Employee employee = new Employee();
        employee.setSeat(SEAT);
        assertEquals(SEAT,employee.getSeat());
    }

    @Test
    void shouldSetEmail() {
        Employee employee = new Employee();
        employee.setEmail(EMAIL);
        assertEquals(EMAIL,employee.getEmail());
    }

    @Test
    void shouldSetPhoneNumber() {
        Employee employee = new Employee();
        employee.setPhoneNumber(PHONE_NUMBER);
        assertEquals(PHONE_NUMBER,employee.getPhoneNumber());
    }
    @Test
    void shouldSetFullName() {
        Employee employee = new Employee();
        employee.setFullName(FULL_NAME);
        assertEquals(FULL_NAME,employee.getFullName());
    }

    @Test
    void shouldSetAge() {
        Employee employee = new Employee();
        employee.setAge(AGE);
        assertEquals(AGE,employee.getAge());
    }

    @Test
    void shouldGiveFullName() {
        Employee employee = new Employee();
        employee.setFullName(FULL_NAME);
        assertEquals(FULL_NAME, employee.getFullName());
    }

    @Test
    void shouldGiveSeat() {
        Employee employee = new Employee();
        employee.setSeat(SEAT);
        assertEquals(SEAT,employee.getSeat());
    }

    @Test
    void shouldGiveEmail() {
        Employee employee = new Employee();
        employee.setEmail(EMAIL);
        assertEquals(EMAIL,employee.getEmail());
    }

    @Test
    void shouldGiveNumber() {
        Employee employee = new Employee();
        employee.setPhoneNumber(PHONE_NUMBER);
        assertEquals(PHONE_NUMBER,employee.getPhoneNumber());
    }

    @Test
    void shouldGiveAge() {
        Employee employee = new Employee();
        employee.setAge(AGE);
        assertEquals(AGE,employee.getAge());
    }
}