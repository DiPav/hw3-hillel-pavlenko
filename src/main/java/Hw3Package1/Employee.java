package Hw3Package1;

public class Employee {
    private String fullName;
    private String seat;
    private String email;
    private String phoneNumber;
    private int age;

    public Employee(String fullName, String seat, String email, String phoneNumber, int age) {
        this.fullName = fullName;
        this.seat = seat;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.age = age;
    }

    public Employee() {

    }

    public void setSeat(String seat) {
        this.seat = seat;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getFullName() {
        return fullName;
    }

    public String getSeat() {
        return seat;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public int getAge() {
        return age;
    }
}
