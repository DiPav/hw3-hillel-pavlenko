package Hw3Package1;

public class Car {
    public void start(){
        if (startElectricity() && startCommand() && startFuelSystem()) System.out.println("Car is ready to go!");
    }

    private boolean startFuelSystem() {
        System.out.println("Starting Fuel System...");
        return true;
    }

    private boolean startCommand() {
        System.out.println("Starting Command...");
        return true;
    }

    private boolean startElectricity() {
        System.out.println("Starting electricity...");
        return true;
    }
}
